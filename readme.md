# Component Boilerplate

## Richtlijnen voor Componenten
- Schrijf Javascript in [AMD modules](http://requirejs.org/docs/whyamd.html#amd).
- Geeft specifieke instellingen mee via de data attributen van de html tag.
- Zorg dat de module geimplementeerd kan worden via [RequireJS](http://requirejs.org/docs/plugins.html).
- Houd je depenencies en versienummer bij via het [Bower manifest bestand (bower.json)](http://bower.io/docs/creating-packages/#bowerjson).	
- Geef elk component een eigen repository.
- Schrijf de Sass van een component volgens de [Applepie richtlijnen](http://www.apppie.org/pages/approach/styleguide.html).
- Laat een demo zien in het index.html bestand


## Features
- List features here.

## Settings
(example below)

Parameter  | Type | Default | Description
---------- | ---- | ------- | -----------
fade       | bool | false   | Should component fade-in

## Methods

### foo: function( foo, bar ){}
Doet foo.
